package com.base.engine;

public class Time {

	public static final long SECOND = (long) 1E9;
	
	private static double delta;
	
	public static long getTime() {
		return System.nanoTime();
	}
	
	public static double getDouble() {
		return delta;
	}
	
	public static void setDelta(double delta) {
		Time.delta = delta;
	}
}
